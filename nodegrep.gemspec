# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name = 'nodegrep'
  s.summary = 'A utility to match patterns in Ruby code'
  s.authors = ['Jon Jenkins']
  s.email = 'jjenkins@gitlab.com'
  s.files = [
    'lib/nodegrep.rb',
    'lib/nodegrep/matcher.rb',
    'lib/nodegrep/presenter.rb'
  ]
  s.version = '0.0.3'
  s.required_ruby_version = '>= 2.6.0'
  s.homepage = 'https://gitlab.com/jon_jenkins/nodegrep'
  s.license = 'MIT'
  s.add_runtime_dependency 'colorize', '~> 0.8'
  s.add_runtime_dependency 'rubocop', '~> 1.40'
  s.add_development_dependency 'pry', '~> 0.14'
  s.add_development_dependency 'rspec', '~> 3.12'
  s.add_development_dependency 'rubocop-rspec', '~> 2.15'
  s.executables << 'nodegrep'
end
