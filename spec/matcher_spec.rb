# frozen_string_literal: true

test_code = File.read('./spec/fixtures/test_code.rb')

RSpec.describe NodeGrep::Matcher do
  context 'valid source' do
    let(:matcher) { described_class.new(test_code) }
    it "doesn't fail" do
      expect { matcher }.to_not raise_error
    end

    it 'matches a thing' do
      matches = matcher.matching_nodes('int')
      expect(matches.length).to be > 0
    end
  end

  context 'invalid source' do
    it 'raises an error' do
      expect { described_class.new('as8 979876&*H^&*(6789') }.to raise_error NodeGrep::Matcher::SourceParseError
    end
  end
end
