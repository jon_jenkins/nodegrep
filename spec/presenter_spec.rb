# frozen_string_literal: true

test_code = File.read('./spec/fixtures/test_code.rb')

RSpec.describe NodeGrep::Presenter do
  let(:presenter) { described_class.new(test_code) }

  context 'with bad line numbers' do
    it 'raises error if x or y are less than 1' do
      expect { presenter.get_lines(-4, 1) }.to raise_error NodeGrep::Presenter::OutOfBounds
    end

    it 'raises error if x is greater than y' do
      expect { presenter.get_lines(65, 1) }.to raise_error NodeGrep::Presenter::OutOfBounds
    end

    it 'raises error if x or y are bigger than source lines' do
      expect { presenter.get_lines(-4, 65_000) }.to raise_error NodeGrep::Presenter::OutOfBounds
    end
  end

  context 'with a single line' do
    context 'with line numbers' do
      it 'presents the line with a number' do
        presenter.line_numbers = true
        expect(presenter.get_lines(5, 5)).to eq '5: require "benchmark"'
      end
    end

    context 'without line numbers' do
      it 'presents the line without a line number' do
        presenter.line_numbers = false
        expect(presenter.get_lines(5, 5)).to eq 'require "benchmark"'
      end
    end
  end

  context 'with multiple lines' do
    context 'without line numbers' do
      it 'presents the lines without numbers' do
        presenter.line_numbers = false
        expect(presenter.get_lines(4, 10)).to eq <<~OUTPUT

          require "benchmark"
          require "set"
          require "zlib"
          require "active_support/core_ext/array/access"
          require "active_support/core_ext/enumerable"
          require "active_support/core_ext/module/attribute_accessors"
        OUTPUT
          .chomp
      end

      context 'with line numbers' do
        it 'presents the lines without numbers' do
          presenter.line_numbers = true
          expect(presenter.get_lines(4, 10)).to eq <<~OUTPUT
            4:#{' '}
            5: require "benchmark"
            6: require "set"
            7: require "zlib"
            8: require "active_support/core_ext/array/access"
            9: require "active_support/core_ext/enumerable"
            10: require "active_support/core_ext/module/attribute_accessors"
          OUTPUT
            .chomp
        end
      end
    end
  end
end
