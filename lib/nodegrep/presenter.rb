# frozen_string_literal: true

require 'colorize'

module NodeGrep
  class Presenter
    class OutOfBounds < StandardError; end

    attr_accessor :color_output, :line_numbers

    @color_output = false
    @line_numbers = false

    def initialize(source)
      @source_lines = source.split("\n").map { |l| l.gsub("\r", '') }
    end

    def get_lines(x, y)
      num_lines = @source_lines.length
      raise OutOfBounds if x < 1 || y < 1 || x > y
      raise OutOfBounds if x > num_lines || y > num_lines

      x -= 1
      y -= 1
      formatted_lines = (x..y).each.collect do |i|
        output = String.new
        output << "#{i + 1}: " if @line_numbers
        output << @source_lines[i]
        output
      end
      formatted_lines.join("\n")
    end
  end
end
