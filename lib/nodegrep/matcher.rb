# frozen_string_literal: true

require 'rubocop'
require 'rubocop-ast'

module NodeGrep
  class Matcher
    class SourceParseError < StandardError; end

    def initialize(source)
      @parsed_source = RuboCop::ProcessedSource.new(source, RUBY_VERSION.to_f)
      @parsed_source.diagnostics.each do |d|
        raise SourceParseError if d.level == :error
      end
    end

    def matching_nodes(pattern)
      @matches = []
      @pattern = RuboCop::NodePattern.new(pattern)
      visit_node(@parsed_source.ast)
      @matches
    end

    private

    def visit_node(node)
      return unless node.class.to_s.include?('RuboCop::AST')

      @matches << node if @pattern.match(node)
      node.children.each { |child| visit_node(child) }
    end
  end
end
