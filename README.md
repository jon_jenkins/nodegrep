# NodeGrep

Find specific patterns of code in Ruby projects. Better than text or regexp matching!

# Installation

```bash
gem install nodegrep
```

## How it works

This gem, when installed, adds the nodegrep script to your local Ruby's bin folder. The script is invoked like so:

```bash
nodegrep [flags] pattern [files]
```

Patterns are given using RuboCop's NodePattern syntax, which matches Ruby code that has been parsed into structured AST data. This is the reference for the NodePattern DSL: https://docs.rubocop.org/rubocop-ast/node_pattern.html

## Flags

| Flag | Description |
| -----| ----------- |
| -l   | Lazy evaluation. Search will stop after first match found. |
| -n   | Print line numbers |
| -r   | Recursive operation. Will recurse through current directory if no path is passed, otherwise will recurse through passed path. |


## Example usage

Recursively find all instances of scripts requiring rubocop:

```bash
cd /path/to/your/project
nodegrep -n -r '(send nil? :require (str "rubocop"))'
```

Find all instances of a method invocation in a Rails project migration folder:

```bash
nodegrep -n '(send nil? :finalize_background_migration ... )' db/migrate/*.rb
```

## Caveats

Source code will be parsed using the ruby version with which you're running. Be aware of this if you are using this utility with a codebase whose required Ruby version differs from the one you are currently running.